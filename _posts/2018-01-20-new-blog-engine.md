---
layout: post
title:  "New Blog Engine!"
date:   2018-01-20 00:17:14 -0600
categories: technical
---
I'm now blogging using [Jekyll][jekyll] static website generator on [GitLab][gitlab]!

Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. 

File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. 

If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

[jekyll]: http://jekyllrb.com/
[gitlab]: https://gitlab.com/
[jekyll-docs]: http://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
